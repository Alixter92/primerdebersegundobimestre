import com.sun.org.apache.xpath.internal.operations.Or;

public class Main {


    public static void main(String[] args) {

        ///casting de Fruit a Apple
        try{
            Fruit fr = new Fruit();
            Apple app= (Apple)fr;
            System.out.println("cheking cast for FruitApple: yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }

        ///casting de Fruit a Sqeezable

        try{
            Fruit fr = new Fruit();
            Sqeezable sq=(Sqeezable) fr;

            System.out.println("cheking cast for FruitSqeezable:yes casting ready");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }
        ///casting de Fruit a Citrus

        try{
            Fruit fr = new Fruit();
            Citrus cr=(Citrus) fr;

            System.out.println("cheking cast for FruitCitrus:yes casting ready");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }
        ///casting de Fruit a Orange

        try{
            Fruit fr = new Fruit();
            Orange or=(Orange) fr;

            System.out.println("cheking cast for FruitOrange:yes casting ready");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }

        ///casting de Apple a Fruit
        try{
            Apple App = new Apple();
            Fruit fr= (Fruit)App;
            System.out.println("cheking cast for AppleFruit: yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }
        ///casting de Apple a Sqeezable
        try{
            Apple ap=new Apple();
            Sqeezable sq =(Sqeezable) ap;

            System.out.println("cheking cast for AppleSqeezable:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }
        ///casting de Apple a Citrus
        /*al hacer este casting nos da un error de compilacion
        try{
            Apple ap =new Apple();
            Citrus cr = (Citrus)ap;

            System.out.println("cheking cast for AppleCitrus:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }*/

        ///casting de Apple a Orange
        /*al hacer este casting nos da un error de compilacion
        try{
            Apple ap=new Apple();
            Orange or=(Orange) ap;

            System.out.println("cheking cast for AppleSqeezable:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }*/

        ///casting de Citrus a Fruit
        try{
            Citrus cr=new Citrus();
            Fruit fr =(Fruit)cr;

            System.out.println("cheking cast for CitrusFruit:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }
        ///casting de Citrus a Apple
        /*al hacer este casting nos da un error de compilacion
        try{
            Citrus cr=new Citrus();
            Apple ap =(Apple)cr;

            System.out.println("cheking cast for CitrusFruit:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }*/

        ///casting de Citrus a Orange
        try{
            Citrus cr=new Citrus();
            Orange or =(Orange) cr;

            System.out.println("cheking cast for CitrusOrange:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }

        ///casting de Citrus a Sqeezable
        try{
            Citrus cr=new Citrus();
            Sqeezable sq =(Sqeezable) cr;

            System.out.println("cheking cast for CitrusSqeezablet:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }


        ///casting de Orange a Fruit
        try{
           Orange or=new Orange();
           Fruit fr =(Fruit)or;

            System.out.println("cheking cast for OrangeFruit:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }

        ///casting de Orange a Apple
        /*Al  realizar este casting nos da un error de compilacion
        try{
            Orange or=new Orange();
            Apple ap =(Apple) or;

            System.out.println("cheking cast for OrangeFruit:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }*/

        ///casting de Orange a Sqeezable
        try{
            Orange or=new Orange();
            Sqeezable sq =(Sqeezable) or;

            System.out.println("cheking cast for OrangeSqeezable:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }
        ///casting de Orange a Citrus
        try{
            Orange or=new Orange();
            Citrus cr=(Citrus) or;

            System.out.println("cheking cast for OrangeCitrus:yes casting ready ");
        }catch (ClassCastException e){

            System.out.println("No casting ready: "+e.getMessage());
        }



    }
}
